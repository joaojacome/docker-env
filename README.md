# Contains

* php 7.0 (fpm)
* nginx
* mariadb

# How To Use
* install with `./tools install`
* Spin up the containers with `./tools docker up`
* Spin down the containers with `./tools docker down`

* execute composer on the backend folder with `./tools composer ...`
* execute nodejs on the frontend folder with `./tools node ...`
* execute php on the backend folder with `./tools php ...`
