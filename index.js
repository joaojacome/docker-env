#!/usr/bin/env node

const tasks = [
  'composer', 'docker', 'php', 'node'
];

const currentFolder = __dirname;

if (!process.argv[2]) {
  console.log('No task specified');
  process.exit(0);
}

if (tasks.indexOf(process.argv[2]) === -1) {
  console.log('Invalid task');
  process.exit(0);
}
let params = process.argv.slice(3,process.argv.length);

require(currentFolder + '/tasks/' + process.argv[2] + '/index.js')(currentFolder, params);
