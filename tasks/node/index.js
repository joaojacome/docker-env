const which = require('which');
const { spawn } = require('child_process');

const task = (basePath, parameters) => {
    let src = process.cwd() + '/src';
    let docker = which.sync('docker');
    let user = process.getuid();
    let group = process.getgid();
    let joinedParameters = parameters.join(' ');
    let command = `run -it --rm \
        -v ${src}:/usr/src/app \
        -w /usr/src/app \
        --user ${user}:${group}  \
        node:9.3.0 ${joinedParameters} `;
    let ps = spawn(docker, [command], { stdio: 'inherit', cwd: basePath,  shell: true});
}
module.exports = task;