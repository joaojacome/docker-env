const which = require('which');
const { spawn } = require('child_process');

const task = (basePath, parameters) => {
    let src = process.cwd() + '/src';
    let docker = which.sync('docker');
    let user = process.getuid();
    let group = process.getgid();
    let joinedParameters = parameters.join(' ');
    let command = `exec -it docker-env_backend_1 php ${joinedParameters} `;
    let ps = spawn(docker, [command], { stdio: 'inherit', cwd: basePath,  shell: true});
}
module.exports = task;