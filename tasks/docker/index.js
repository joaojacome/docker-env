const which = require('which');
const { spawn } = require('child_process');

const task = (basePath, parameters) => {
    let docker = which.sync('docker-compose');
    let command = '';
    if (parameters[0] === 'up') {
        command = 'up -d';
    } else if (parameters[0] === 'down') {
        command = 'down';
    } else if (parameters[0] === 'restart') {
        command = 'restart';
    } else {
        process.exit();
    }
    let ps = spawn(docker, [command], { stdio: 'inherit', cwd: basePath,  shell: true, env: {
        UID: process.getuid(),
        GID: process.getgid(),
    }});
}
module.exports = task;